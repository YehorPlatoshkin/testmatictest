package figures;

public class Circle extends Figure{

    public Double getRadius(){
        return Math.sqrt(getSquare()/ Math.PI);
    }

    @Override
    public String toString() {
        return getType() + ", square : " + getSquare()
                + ", radius : " + getRadius() + ", color : " + getColor();
    }
}
