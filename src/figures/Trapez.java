package figures;

public class Trapez extends Figure{
    @Override
    public String toString() {
        return getType() + ", square : " + getSquare()
                + ", color : " + getColor();
    }
}
