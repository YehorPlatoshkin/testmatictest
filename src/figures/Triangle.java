package figures;

public class Triangle extends Figure{

    public Double getHypotenuse(){
        return 2*Math.sqrt(getSquare());
    }

    @Override
    public String toString() {
        return getType() + ", square : " + getSquare()
                + ", hypotenuse : " + getHypotenuse() + ", color : " + getColor();
    }
}
