package figures;

public class Figure {
    private String type = this.getClass().getSimpleName();
    private Double square;
    private String color;

    public String getType() {
        return type;
    }

    public Double getSquare() {
        return square;
    }

    public void setSquare(Double square) {
        this.square = square;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
