package figures;

public class Square extends Figure {

    public Double getSideLength(){
        return Math.sqrt(getSquare());
    }

    @Override
    public String toString() {
        return getType() + ", square : " + getSquare()
                + ", side length : " + getSideLength()
                + ", color : " + getColor();
    }
}
