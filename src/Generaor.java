import figures.*;

import java.util.Random;

class Generaor{
    private final Class[] classes = new Class[]{Triangle.class,Trapez.class,
            Circle.class, Square.class};
    private final String[] colors = new String[]{"red", "blue",
            "yellow", "green", "black"};

    private final Random random = new Random();
    public Figure createFigure() throws IllegalAccessException, InstantiationException {
        Figure figure = (Figure) classes[random.nextInt(classes.length)]
                .newInstance();
        figure.setColor(colors[random.nextInt(colors.length)]);
        figure.setSquare((double) random.nextInt(8784));
        return figure;
    }
}
