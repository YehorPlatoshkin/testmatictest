import figures.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        Random random = new Random();
        List<Figure> figures = new ArrayList<>();
        for (int i = 0; i < random.nextInt(7777); i++){
            figures.add(new Generaor().createFigure());
        }
        for (Figure figure : figures){
            System.out.println("Figure : " + figure.toString());
        }
    }
}
